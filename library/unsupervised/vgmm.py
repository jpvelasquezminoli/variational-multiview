from scipy.special import digamma, gamma, logsumexp
import numpy as np
from library.unsupervised.kmeans import KMeans

class VGMM:
    def __init__(self, clusters=2, it=500):
        """
            Initialize parameters
            Parameters:
                clusters: int
                          number of desire clusters
                it: int
                    maximum number of iterations
            Returns: 
                Nothing
        """
        self.clusters=clusters
        self.it=it
    
    def assignParams(self,X):
        n, self.p=X.shape
        self.alpha0=np.ones(self.clusters)*1/self.clusters
        self.m0=np.mean(X, axis=0)
        self.W0=np.eye(self.p)
        self.v0=self.p
        self.beta0=1

        self.Nk=(n/self.clusters)+np.zeros(self.clusters)
        self.alpha =self.alpha0+self.Nk
        self.beta=self.beta0+self.Nk
        kmeans=KMeans(clusters=self.clusters)
        kmeans.fit(X)
        self.mu=kmeans.getModel()
        self.W=np.tile(self.W0, (self.clusters,1,1))
        self.v=self.v0+self.Nk

    def expectation(self,X):
        d=X[:, None, :] - self.mu
        f=np.sum(np.einsum("kij,nkj->nki", self.W, d) * d, axis=-1)
        expectation=-0.5*(self.p/self.beta+self.v*f)     
        ln_pi=digamma(self.alpha)-digamma(self.alpha.sum())
        s=digamma(0.5*(self.v - np.arange(self.p)[:, None])).sum(axis=0)
        ln_Lambda=s+self.p*np.log(2)+np.linalg.slogdet(self.W)[1]
        ln_ro=ln_pi+0.5*ln_Lambda+expectation
        ln_r=ln_ro-logsumexp(ln_ro, axis=-1)[:, None]
        r = np.exp(ln_r)
        return r
    
    def maximization(self,X,r):
        self.Nk=r.sum(axis=0)
        Xe=(X.T.dot(r)/self.Nk).T
        d=X[:, None, :]-Xe
        S=np.einsum('nki,nkj->kij', d, r[:, :, None] * d)/self.Nk[:, None, None]
        self.alpha=self.alpha0 + self.Nk
        self.beta=self.beta0 + self.Nk
        self.mu=(self.beta0*self.m0+self.Nk[:, None]*Xe)/self.beta[:, None]
        d=Xe-self.m0
        f=np.linalg.inv(self.W0)+(self.Nk*S.T).T
        s=(self.beta0*self.Nk*np.einsum('ki,kj->kij', d, d).T/(self.beta0 + self.Nk)).T
        self.W = np.linalg.inv(f+s)
        self.v = self.v0 + self.Nk

    def fit(self,X):
        self.assignParams(X)
        theta = np.hstack(
            (self.alpha.ravel(),
             self.beta.ravel(),
             self.mu.ravel(),
             self.W.ravel(),
             self.v.ravel())
        )
        for _ in range(self.it):
            r=self.expectation(X)
            self.maximization(X,r)
            newTheta = np.hstack(
            (self.alpha.ravel(),
             self.beta.ravel(),
             self.mu.ravel(),
             self.W.ravel(),
             self.v.ravel())
            )
            if np.allclose(theta, newTheta):
                break
            else:
                theta = newTheta

    def transform(self, X):
        """
            Evaluate samples with the model
            Parameters:
                X: (n_samples,n_features) numpy array
                   Data 
            Returns: 
                r: (n_samples,1) numpy array
                   Indicates labels
        """
        return np.argmax(self.expectation(X),axis=1)

    def fit_transform(self, X):
        """
            Perform fit and transform together
            Parameters:
                X: (n_samples,n_features) numpy array
                   Data 
            Returns: 
                dist: (n_samples,1) numpy array
                   Indicates labels
        """
        self.fit(X)
        return self.transform(X)