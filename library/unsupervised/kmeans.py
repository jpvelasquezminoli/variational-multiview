# -*- coding: utf-8 -*-
"""
    Created By: JPMinoli
    Year: 2020
    This module allows to implement clustering using k-means
"""
import numpy as np
from scipy.spatial.distance import cdist

class KMeans():
    def __init__(self, clusters=2, it=200):
        """
            Initialize parameters
            Parameters:
                clusters: int
                          number of desire clusters
                it: int
                    maximum number of iterations
            Returns: 
                Nothing
        """
        self.clusters=clusters
        self.it=it

    def getModel(self):
        """
            Return the calculated model
            Parameters:
                Nothing
            Returns: 
                mu: (clusters,n_features) numpy array
                    the model
        """
        return self.mu

    def fit(self, X):
        """
            Calculate the model
            Parameters:
                X: (n_samples,n_features) numpy array
                   Data 
            Returns: 
                Nothing
        """
        I=np.eye(self.clusters)
        mu=X[np.random.choice(len(X), self.clusters, replace=False)]
        for _ in range(self.it):
            muold=np.copy(mu)
            dist=cdist(X,mu)
            index=np.argmin(dist,axis=1)
            r=I[index]
            num=r.T.dot(X)
            den=np.sum(r,axis=0).reshape(-1,1)
            mu=num/den
            if np.allclose(muold,mu):
                break
        self.mu=mu
    
    def transform(self, X):
        """
            Evaluate samples with the model
            Parameters:
                X: (n_samples,n_features) numpy array
                   Data 
            Returns: 
                dist: (n_samples,1) numpy array
                   Indicates labels
        """
        dist=cdist(X,self.mu)
        return np.argmin(dist,axis=1)

    def fit_transform(self, X):
        """
            Perform fit and transform together
            Parameters:
                X: (n_samples,n_features) numpy array
                   Data 
            Returns: 
                dist: (n_samples,1) numpy array
                   Indicates labels
        """
        self.fit(X)
        return self.transform(X)
