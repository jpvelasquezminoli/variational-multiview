# -*- coding: utf-8 -*-
"""
    Created By: JPMinoli
    Year: 2020
    This module allows to implement clustering using gaussian mixture model
"""

import numpy as np
from library.unsupervised.kmeans import KMeans

class GMM():
    def __init__(self, clusters=2, it=500):
        """
            Initialize parameters
            Parameters:
                clusters: int
                          number of desire clusters
                it: int
                    maximum number of iterations
            Returns: 
                Nothing
        """
        self.clusters=clusters
        self.it=it
    
    def getModel(self):
        """
            Return the calculated model
            Parameters:
                Nothing
            Returns: 
                theta: list with mu-media matrix(clusters,nfeatures),
                                 cov-covariances matrices(clusters,nfeatures,nfeatures),
                                 pik-coef of every gaussian(clusters,)
                    the model
        """
        theta=[self.mu,self.cov,self.pik]
        return theta

    def gaussian(self,X):
        k=self.mu.shape[0]
        n=len(X)
        nfeature=X.shape[1]
        g=np.ones((n,k))
        for i in range(n):
            for j in range(k):
                d = (X[i,:]-self.mu[j,:]).reshape(1,-1)
                pre=np.linalg.inv(self.cov[j,:,:])
                first=d.dot(pre)
                second=first.dot(d.T)
                third=np.exp(-0.5*second)
                fourth=1/np.sqrt(np.linalg.det(self.cov[j,:,:])*(2*np.pi)**nfeature)
                g[i,j]=fourth*third
        return g

    def expectation(self,X):
        g=self.gaussian(X)
        num=self.pik*g
        den=np.sum(num,axis=1, keepdims=True)
        r=num/den
        return r

    def maximization(self,X,r):
        N=len(X)
        Nk=np.sum(r,axis=0)
        self.pik=Nk/N
        self.mu=(X.T.dot(r)/Nk).T
        nfeature=X.shape[1]
        for i in range(self.clusters):
            acu=np.zeros((nfeature,nfeature))
            for j in range(N):
                d=X[j,:]-self.mu[i,:]
                d=d.reshape(1,-1)
                first=r[j,i]*d.T.dot(d)
                acu=acu+first
            self.cov[i,:,:]=acu/Nk[i]

    def fit(self, X):
        """
            Calculate the model
            Parameters:
                X: (n_samples,n_features) numpy array
                   Data 
            Returns: 
                Nothing
        """
        kmeans=KMeans(clusters=self.clusters)
        kmeans.fit(X)
        self.mu=kmeans.getModel()
        cov=np.cov(X.T)
        self.cov=np.array([cov for _ in range(self.clusters)])
        self.pik=np.ones(self.clusters)/len(X)

        theta = np.hstack(
            (self.mu.ravel(),
             self.cov.ravel(),
             self.pik.ravel())
        )
        for _ in range(self.it):
            r=self.expectation(X)
            self.maximization(X,r)
            newTheta = np.hstack(
                    (self.mu.ravel(),
                    self.cov.ravel(),
                    self.pik.ravel())
                )
            if np.allclose(theta, newTheta):
                break
            else:
                theta = newTheta
    
    def transform(self, X):
        """
            Evaluate samples with the model
            Parameters:
                X: (n_samples,n_features) numpy array
                   Data 
            Returns: 
                r: (n_samples,1) numpy array
                   Indicates labels
        """
        return np.argmax(self.expectation(X),axis=1)

    def fit_transform(self, X):
        """
            Perform fit and transform together
            Parameters:
                X: (n_samples,n_features) numpy array
                   Data 
            Returns: 
                dist: (n_samples,1) numpy array
                   Indicates labels
        """
        self.fit(X)
        return self.transform(X)